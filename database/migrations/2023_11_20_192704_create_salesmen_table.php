<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('salesmen', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->json('titles_before')->nullable();
            $table->json('titles_after')->nullable();
            $table->integer('prosight_id');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('gender');
            $table->string('marital_status')->nullable();
            $table->primary('id');




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('salesmen');
    }
};
