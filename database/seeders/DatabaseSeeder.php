<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Salesman;
use App\Enums\TitlesBefore;
use App\Enums\TitlesAfter;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Salesman::truncate();
        
        $csvFile = fopen(base_path("database/data/salesmen.csv"), "r");
        
        $firstline = true;
        
        while (($data = fgetcsv($csvFile, 2000, ";")) !== FALSE) {
            if (!$firstline) {
                
                Salesman::create([
                    "first_name" => $data['0'],
                    "last_name" => $data['1'],
                    "titles_before" => empty($data['2'])? null : TitlesBefore::getKey($data['2']),
                    "titles_after" =>  empty($data['3'])? null : TitlesAfter::getKey($data['3']),
                    "prosight_id" => $data['4'],
                    "email" => $data['5'],
                    "phone" => $data['6'],
                    "gender" => $data['7'],
                    "marital_status" => $data['8']
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);

    }
}
