<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static Gender Male()
 * @method static Gender Female()
 */
final class Gender extends Enum
{
    const Male = 'm';
    const Female = 'f';
}
