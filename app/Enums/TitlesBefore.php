<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static TitlesBefore Bc()
 * @method static TitlesBefore Mgr()
 * @method static TitlesBefore Ing()
 * @method static TitlesBefore JUDr()
 * @method static TitlesBefore MVDr()
 * @method static TitlesBefore MUDr()
 * @method static TitlesBefore PaedDr()
 * @method static TitlesBefore Prof()
 * @method static TitlesBefore Doc()
 * @method static TitlesBefore Dipl()
 * @method static TitlesBefore MDDr()
 * @method static TitlesBefore Dr()
 * @method static TitlesBefore MgrArt()
 * @method static TitlesBefore ThLic()
 * @method static TitlesBefore PhDr()
 * @method static TitlesBefore PhMr()
 * @method static TitlesBefore RNDr()
 * @method static TitlesBefore ThDr()
 * @method static TitlesBefore RSDr()
 * @method static TitlesBefore Arch()
 * @method static TitlesBefore PharmDr()
 */
final class TitlesBefore extends Enum
{
    const Bc = 'Bc.';
    const Mgr = 'Mgr.';
    const Ing = 'Ing.';
    const JUDr = 'JUDr.';
    const MVDr = 'MVDr.';
    const MUDr = 'MUDr.';
    const PaedDr = 'PaedDr.';
    const Prof = 'prof.';
    const Doc = 'doc.';
    const Dipl = 'dipl.';
    const MDDr = 'MDDr.';
    const Dr = 'Dr.';
    const MgrArt = 'Mgr. art.';
    const ThLic = 'ThLic.';
    const PhDr = 'PhDr.';
    const PhMr = 'PhMr.';
    const RNDr = 'RNDr.';
    const ThDr = 'ThDr.';
    const RSDr = 'RSDr.';
    const Arch = 'arch.';
    const PharmDr = 'PharmDr.';

 

}