<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static TitlesAfter CSc()
 * @method static TitlesAfter DrSc()
 * @method static TitlesAfter PhD()
 * @method static TitlesAfter ArtD()
 * @method static TitlesAfter DiS()
 * @method static TitlesAfter DiSart()
 * @method static TitlesAfter FEBO()
 * @method static TitlesAfter MPH()
 * @method static TitlesAfter BSBA()
 * @method static TitlesAfter MBA()
 * @method static TitlesAfter DBA()
 * @method static TitlesAfter MHA()
 * @method static TitlesAfter FCCA()
 * @method static TitlesAfter MSc()
 * @method static TitlesAfter FEBU()
 * @method static TitlesAfter LLM()
 */
final class TitlesAfter extends Enum
{
    const CSc = 'CSc.';
    const DrSc = 'DrSc.';
    const PhD = 'PhD.';
    const ArtD = 'ArtD.';
    const DiS = 'DiS';
    const DiSart = 'DiS.art';
    const FEBO = 'FEBO';
    const MPH = 'MPH';
    const BSBA = 'BSBA';
    const MBA = 'MBA';
    const DBA = 'DBA';
    const MHA = 'MHA';
    const FCCA = 'FCCA';
    const MSc = 'MSc.';
    const FEBU = 'FEBU';
    const LLM = 'LL.M';
}
