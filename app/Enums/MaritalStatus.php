<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static MaritalStatus Single()
 * @method static MaritalStatus Married()
 * @method static MaritalStatus Divorced()
 * @method static MaritalStatus Widowed()
 */
final class MaritalStatus extends Enum
{
    const Single = 'single';
    const Married = 'married';
    const Divorced = 'divorced';
    const Widowed = 'widowed';
}
