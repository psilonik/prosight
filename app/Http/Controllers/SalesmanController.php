<?php

namespace App\Http\Controllers;

use App\Http\Resources\SalesmanCollection;
use App\Http\Requests\StoreSalesmanRequest;
use App\Http\Requests\UpdateSalesmanRequest;
use App\Models\Salesman;
use App\Http\Resources\SalesmanResource;
use Spatie\QueryBuilder\QueryBuilder;

class SalesmanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $salesmen = QueryBuilder::for(Salesman::class)
        ->defaultSort('created_at')
        ->allowedSorts('first_name', 'last_name', 'prosight_id', 'email', 'phone', 'gender')
        ->paginate(request()->get('per_page'))
        ->appends(request()->query());

        return new SalesmanCollection($salesmen);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSalesmanRequest $request)
    {
        $validated = $request->validated();
        $salesman = Salesman::create($validated);

        return new SalesmanResource($salesman);
    }

    /**
     * Display the specified resource.
     */
    public function show(Salesman $salesman)
    {
        return new SalesmanResource($salesman);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Salesman $salesman)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSalesmanRequest $request, Salesman $salesman)
    {
        $validated = $request->validated();
        $salesman->update($validated);

        return new SalesmanResource($salesman);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Salesman $salesman)
    {
        $salesman->delete();

        return response()->noContent();
    }
}
