<?php

namespace App\Http\Controllers;

use App\Models\TitlesBefore;
use Illuminate\Http\Request;

class TitlesBeforeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(TitlesBefore $titlesBefore)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TitlesBefore $titlesBefore)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, TitlesBefore $titlesBefore)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TitlesBefore $titlesBefore)
    {
        //
    }
}
