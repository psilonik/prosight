<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SalesmanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = parent::toArray($request);
        $name =[];
        if(!empty($this->first_name)) {
            $name[] = $this->first_name;
        }
        if(!empty($this->last_name)) {
            $name[] = $this->last_name;
        }

        $data['display_name'] = implode(' ', $name);
        $data['self'] = '/salesmen/'.$this->id;

        return $data;
    }
}
