<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use BenSampo\Enum\Rules\EnumValue;
use BenSampo\Enum\Rules\EnumKey;
use BenSampo\Enum\Rules\Enum;
use App\Enums\MaritalStatus;
use App\Enums\Gender;
use App\Enums\TitlesAfter;
use App\Enums\TitlesBefore;

class UpdateSalesmanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
           'first_name' => 'sometimes|required|string',
            'last_name' => 'sometimes|required|string',
            // 'titles_before.*' => ['string', Rule::in(TitlesBefore::getValues())],
            'titles_before.*' => ['sometimes','string', new EnumKey(TitlesBefore::class)],
            'titles_after.*' => ['sometimes','string', new EnumKey(TitlesAfter::class)],
            'prosight_id' => 'sometimes|required|string',
            'email' => 'sometimes|required|string',
            'phone' => 'sometimes|string',
            'gender' => ['sometimes','required', 'string',new EnumValue(Gender::class)],
            'marital_status' => ['sometimes','string', new EnumValue(MaritalStatus::class)]


        ];
    }


    public function validationData()
    {
        $data = parent::validationData();

        
        if ($this->has('titles_before')) {
            $data['titles_before'] = array_map(function ($value) {
                return TitlesBefore::getKey($value);
            }, $this->input('titles_before'));
        }

        if ($this->has('titles_after')) {
            $data['titles_after'] = array_map(function ($value) {
                return TitlesAfter::getKey($value);
            }, $this->input('titles_after'));
        }

        return $data;
    }    

}
