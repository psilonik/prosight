<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use BenSampo\Enum\Rules\EnumValue;
use BenSampo\Enum\Rules\EnumKey;
use BenSampo\Enum\Rules\Enum;
use App\Enums\MaritalStatus;
use App\Enums\Gender;
use App\Enums\TitlesAfter;
use App\Enums\TitlesBefore;

class StoreSalesmanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            // 'titles_before.*' => ['string', Rule::in(TitlesBefore::getValues())],
            'titles_before.*' => ['string', new EnumKey(TitlesBefore::class)],
            'titles_after.*' => ['string', new EnumKey(TitlesAfter::class)],
            'prosight_id' => 'required|string',
            'email' => 'required|string',
            'phone' => 'string',
            'gender' => ['required', 'string',new EnumValue(Gender::class)],
            'marital_status' => ['string', new EnumValue(MaritalStatus::class)]

        ];
    }

    public function validationData()
    {
        $data = parent::validationData();

        
        if ($this->has('titles_before')) {
            $data['titles_before'] = array_map(function ($value) {
                return TitlesBefore::getKey($value);
            }, $this->input('titles_before'));
        }

        if ($this->has('titles_after')) {
            $data['titles_after'] = array_map(function ($value) {
                return TitlesAfter::getKey($value);
            }, $this->input('titles_after'));
        }

        return $data;
    }    

}
