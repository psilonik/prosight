<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\AsEnumCollection;
use App\Enums\TitlesBefore;
use App\Enums\TitlesAfter;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class Salesman extends Model
{
    use HasFactory;
    use HasUuids;

    protected $casts = [
        'titles_before' => AsEnumCollection::class . ':' . TitlesBefore::class,
        'titles_after' => AsEnumCollection::class . ':' . TitlesAfter::class
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'titles_before',
        'titles_after',
        'prosight_id',
        'email',
        'phone',
        'gender',
        'marital_status'
    ];

    // public function before_titles(): BelongsToMany
    // {
    //     return $this->belongsToMany(BeforeTitle::class, 'before_title_salesman');
    // }


}
